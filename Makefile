.PHONY: clean dist
python=python3
VERSION="0.1"
BUILD=$(shell date +%Y%m%d%H%M)
NAME=sibrt-led-display
PKG=${NAME}-${VERSION}-${BUILD}
DPKG_ARCH:=armhf
DPKG=dpkg-deb -Zgzip


all:
dist: release/${PKG}.deb

build/${PKG}:
	mkdir -p $@/usr/share/${NAME}
	mkdir -p $@/usr/lib/systemd/system/
	mkdir -p $@/usr/share/${NAME}/sibrtclient
	cp sibrtclient/__init__.py $@/usr/share/${NAME}/sibrtclient/
	cp sibrtclient/client.py $@/usr/share/${NAME}/sibrtclient/
	cp display.py $@/usr/share/${NAME}/display.py
	chmod 755 $@/usr/share/${NAME}/display.py
	cp dist/${NAME}.service $@/usr/lib/systemd/system/
	mkdir -p $@/DEBIAN
	echo "systemctl daemon-reload" > $@/DEBIAN/postinst
	chmod 755 $@/DEBIAN/postinst
	cp dist/dpkg-control $@/DEBIAN/control
	echo Version: ${VERSION}-${BUILD} >> $@/DEBIAN/control
	echo Architecture: ${DPKG_ARCH} >> $@/DEBIAN/control



release/${PKG}.deb: build/${PKG}
	mkdir -p release
	${DPKG} -b $< $@


clean:
	rm -rf static
	rm -rf build
