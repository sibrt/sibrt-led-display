#!/usr/bin/env python3

import time
import board
from adafruit_ht16k33.segments import Seg7x4
from sibrtclient.client import Client

status=False

class Display:
    def __init__(self):
        self.i2c = board.I2C()
        self.display = Seg7x4(self.i2c)
        self.display.colon=False
        self.display.blink_rate=0
        self.status=False
    def update(self,val):
        self.status=(self.status==False)
        a=0
        if("value" in val.keys()):
            if(val['value']!=None):
                a="%2.2f"%val['value']
                if(self.status==True):
                    a+="."
            self.display.print(a)
    def test(self):
        self.display.print("88.88")
        time.sleep(4)
        self.display.print("12.34")
        time.sleep(4)

display=Display()
display.test()
client=Client(server="127.0.0.1:80")
while(True):
    stream=client.ConnectStream("temp1")
    stream.OnUpdate(display.update)
    stream.Wait()
    print("lost connection, reconnect in 5s")
    display.update({'value':0})
    time.sleep(5)
